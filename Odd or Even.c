#include <stdio.h>
int main () {
    int num;
    printf("Enter an Integer: ");
    scanf("%d", &num);

    //true if num is perfectly divisible by 2
    if (num % 2 == 0)
        printf("%d is an Even Number", num);
    else
        printf("%d is an Odd Number", num);

    return 0;

}
